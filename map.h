#pragma once
#include "personnage.h"

class map
{
	friend class personnage;
	int taille;
	int** grille;
public:
	map();
	map(int **);
	~map();
	void display(int, int);
	void position(int,int);
	int ** getGrille();
	int getTaille();
};

