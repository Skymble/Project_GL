#include <iostream>
#include "personnage.h"
#include "string.h"
#include "map.h"


using namespace std;

Personnage::Personnage(string nNom, int nhp, int ndegat, int ncomp, int nmana)
{
	nom = nNom;
	hpMax = nhp;
	hp = nhp;
	degat = ndegat;
	comp = ncomp;
	mana = nmana;
	manaMax = nmana;
	x = 8;
	y = 5;

/*	for(int i = 0; i < 25; i++)
	{
		for(int j = 0; j < 25; j++)
		{
			if(m[i][j] == 2)
			{
				x = j;
				y = i;
			}
		}
	}
*/
}

Personnage::~Personnage()
{
	return;
}

void Personnage::affiche()
{
	cout << nom << " nombre de hp : " << hp << endl;
}

void Personnage::attaque(int & hpJ2) 	// La fonction attaque prend en argument les hp du personnage qui va se faire frapper
{										// Elle est appelé via l'attaquant
	hpJ2 -= degat;						// j1.attaque(j2.hp) -> j1 attaque les hp de j2
	if(hpJ2 < 0)
	{
		hpJ2 = 0;
	}
}

bool Personnage::competence(int & hpJ2)				// la fonction vérifie si la compétence peut être lancé, et compte le cout en mana
{
	if(comp == 1 && mana >= 5)						// La compétence 1 inflige le double des dégats de bases
	{
		hpJ2 -= degat*2;
		mana -= 5;
		if(hpJ2 < 0)
		{
			hpJ2 = 0;
		}
		return true;
	}
	else if(comp == 2 && mana >= 5)					// La compétence 2 rend la moitié des dégats infligés en point de vie
	{
		hpJ2 -= degat;
		hp += degat/2;
		if(hp > hpMax)
		{
			hp = hpMax;
		}
		mana -= 5;
		if(hpJ2 < 0)
		{
			hpJ2 = 0;
		}
		return true;
	}
	else if(comp == 3 && mana >= 5)					// La compétence 3 inflige davantage de dégats si le lanceur a perdu des hp
	{
		hpJ2 -= degat + (hpMax - hp);
		mana -= 5;
		if(hpJ2 < 0)
		{
			hpJ2 = 0;
		}
		return true;
	}
	return false;
}

void Personnage::combat(Personnage j2)												// La fonction combat est appelé lors d'un combat
{																					// Il y a le choix entre attaquer, défendre, et, pour plus tard, utiliser la compétence
	bool fin = false;
	bool tour = false;
	int choix;
	while(!fin)
	{
		cout << "attaquer(1), défendre(2) ou compétence(3) ?";
		cin >> choix;
		if(choix != 1 && choix != 2 && choix != 3)												
		{																			
			cout << "Incorrect, veuillez rentrer à nouveau votre choix" << endl;	
			cout << "attaquer(1), défendre(2) ou compétence(3) ?";
			cin >> choix;
		}

		if(choix == 1)																// Choisir attaquer permettra à votre personnage d'occasioner des dégats
		{																			// mais il en subira car il ne pourra pas se défendre si son adversaire l'attaque
			cout << nom << " attaque " << j2.nom << endl;
			attaque(j2.hp);
			cout << j2.nom << " subis " << degat << " point de dégats" << endl;
			if(j2.hp == 0)
			{
				fin = true;
				cout << endl << nom << " win" << endl;
				break;
			}
			else
			{
				cout << j2.nom << " attaque " << nom << endl;
				j2.attaque(hp);
				
				cout << nom << " subis " << j2.degat << " point de dégats" << endl;
					if(hp == 0)
					{
						fin = true;
						cout << j2.nom << " win" << endl;
						break;
					}
				}
			}
		else if(choix == 2)															// Si vous choisissez de vous défendre, votre personnage
		{																			// pourra parer le coup de votre adversaire, mais en 
			cout << j2.nom << " attaque !"<< endl;									// échange, il ne pourra pas attaquer
			cout << nom << " se défend et ne prend pas de dégats" << endl;
		}
		else if(choix == 3)
		{
			if(competence(j2.hp))
			{
				cout << nom << " utilise sa compétence " << endl;
				competence(j2.hp);
				if(j2.hp == 0)
				{
					fin = true;
					cout << endl << nom << " win" << endl;
					break;
				}
				else
				{
					cout << j2.nom << " attaque " << nom << endl;
					j2.attaque(hp);
					
					cout << nom << " subis " << j2.degat << " point de dégats" << endl;
					if(hp == 0)
					{
						fin = true;
						cout << j2.nom << " win" << endl;
						break;
					}
				}
			}
			else
			{
				cout << "PAS ASSEZ DE MANA" << endl;
			}
		}
		cout << endl;
		affiche();
		j2.affiche();
		cout << endl;
	}
}

int Personnage::getX()
{
	return x;
}

int Personnage::getY()
{
	return y;
}

void display(int ** carte)
{
	for(int i = 0; i < 10; i++)
	{
		for(int j = 0; j < 10; j++)
		{
			cout << carte[i][j] << " ";
		}
		cout << endl;
	}
}

void Personnage::move(int ** map, int taille)
{
	char c;
	cout<<"z,q,s,d ?(haut,gauche,bas,droit) : ";
	cin>>c;
	cout<<endl;
	while(c!='z' && c!= 'Z' && c!='q' && c != 'Q' && c!='s' && c != 'S' && c!='d' && c != 'D')
	{
		cout<<" erreur : z,q,s,d ?(haut,gauche,bas,droit) : ";
		cin>>c;
		cout<<endl;
	}
	
	if(c == 'z' || c== 'Z')
	{
		if(y > 0 && map[x][y-1] == 0)
		{
			y-=1;
			if(mana < manaMax)
			{
				mana+=1;
			}
		}
	}
	if(c == 'q' || c == 'Q')
	{
		if(x > 0 && map[x-1][y] == 0)
		{
			x-=1;
			if(mana < manaMax)
			{
				mana+=1;
			}
		}
		
	}
	if(c == 's' || c == 'S')
	{
		if(y < taille-1 && map[x][y+1] == 0)					// Valeur a changer plus tard (taille-1)
		{
			y+=1;
			if(mana < manaMax)
			{
				mana+=1;
			}
		}
	}
	if(c == 'd' || c == 'D')
	{
		if(x < taille-1 && map[x+1][y] == 0)					// Valeur à changer plus tard
		{
			x+=1;
			if(mana < manaMax)
			{
				mana+=1;
			}
		}
	}
	return;
}

void Personnage::test()
{
	cout << x << endl;
	cout << y << endl;
}