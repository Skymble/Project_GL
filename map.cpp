#include <iostream>
#include <stdlib.h>
#include <string>
#include "map.h"
#include "personnage.h"

using namespace std;

map::map()
{
	taille=10;
	grille=new int*[taille];
	for(int i=0;i<taille;i++)
	{
		grille[i]=new int[taille];
	}
	
	for(int i=0;i<taille;i++)
	{
		for(int j=0;j<taille;j++)
		{
			grille[i][j]=0;
		}
	}
	grille[1][1] = 2;
}

map::map(int ** nMap)
{
	taille = 10;
	grille = new int * [taille];
	for(int i = 0; i < taille; i++)
	{
		grille[i] = new int[taille];
	}
	for(int i = 0; i < taille; i++)
	{
		for(int j = 0; j < taille; j++)
		{
			grille[i][j] = nMap[i][j];
		}
	}
}

map::~map()
{
	for(int i = 0; i < taille; i++)
	{
		delete [] grille[i];
	}
	delete [] grille;
}

void map::display(int x, int y)
{
	system("clear");
	for(int i = 0; i < taille; i++)
	{
		for(int j = 0; j < taille; j++)
		{
			if(x == j && y == i)
			{
				cout << 1 << " ";
			}
			else
				cout << grille[i][j] << " ";
		}
		cout << endl;
	}
}
/*
void map::position(int x, int y)
{
	for(int i = 0; i < taille; i++)
	{
		for(int j = 0; j < taille; j++)
		{
			if(x == j && y == i)
			{
				grille[i][j] = 1;
			}
		}
	}
}
*/
int ** map::getGrille()
{
	return grille;
}

int map::getTaille()
{
	return taille;
}