#pragma once
#include <string>
#include "map.h"

using namespace std;

class Personnage
{
	friend class map;
	string nom;
	int hp;
	int hpMax;
	int degat;
	int x;
	int y;
	int comp;
	int mana;
	int manaMax;

public:
	Personnage(string,int, int, int, int);
	~Personnage();
	void affiche();
	void combat(Personnage);
	void attaque(int &);
	int getX();
	int getY();
	void move(int **, int);
	void test();
	bool competence(int &);
};