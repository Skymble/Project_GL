jeuxGL: main.cpp map.o personnage.o 
	g++ -o jeuxGL main.cpp personnage.o map.o

map.o:	map.cpp map.h 
	g++ -c map.cpp map.h

personnage.o: personnage.cpp personnage.h
	g++ -c personnage.cpp personnage.h

clean:
	rm * .o havannah